import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  characters: any;
  filterCharacter = '';
  constructor(private _http: HttpService) {}

  ngOnInit() {
    this._http.getHP().subscribe((data) => {
      this.characters = data;
      console.log(this.characters);
    });
  }

  getHouseBackground(house: string): string {
    switch (house) {
      case 'Gryffindor':
        return 'gryffindor';
      case 'Slytherin':
        return 'slytherin';
      case 'Ravenclaw':
        return 'ravenclaw';
      case 'Hufflepuff':
        return 'hufflepuff';
      default:
        return 'none';
    }
  }
}
