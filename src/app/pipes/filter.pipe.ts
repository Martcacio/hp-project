import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter',
})
export class FilterPipe implements PipeTransform {
  transform(value: any, ...arg: any[]): any {
    //if (arg.length < 3) return value;
    const resultCharacter = [];
    for (const character of value) {
      if (character.name.toLowerCase().indexOf(arg) > -1) {
        resultCharacter.push(character);
        console.log('sip');
      }
    }
    return resultCharacter;
  }
}
