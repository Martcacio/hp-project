import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  constructor(private http: HttpClient) {}

  getHP() {
    return this.http.get('http://hp-api.herokuapp.com/api/characters ');
  }
}
